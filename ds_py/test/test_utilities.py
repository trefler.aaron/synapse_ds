"""Test for utility functions"""

import sys
sys.path.append('/home/hadoop/common_git_repos/synapse_ds')

from ds_py import utilities as util

def test_function_prototype():
    assert util.function_prototype() < 3
