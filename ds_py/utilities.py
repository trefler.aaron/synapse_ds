"""Utility functions"""
import numpy as np
import pandas as pd


def function_prototype():
    print("This is the prototype function")

    return 1


def create_pandas_df():
    """Create Pandas DataFrame"""
    return pd.DataFrame(np.random.rand(3,2))


def create_spark_df(spark):
    """Create Spark DataFrame"""
    pd_df = pd.DataFrame(np.random.rand(3,2))
    return spark.createDataFrame(pd_df)

