#!/bin/bash
# Basic setup 
sudo pip-3.6 install setuptools --upgrade
sudo pip-3.6 install pyyaml ua-parser user-agents
sudo yum install tmux -y

# Enable spark.ipyspark interpreter in Zeppelin
sudo pip-3.6 install jupyter
sudo pip-3.6 install grpcio
sudo pip-3.6 install protobuf

# Pandas
sudo pip-3.6 install pandas
sudo pip-3.6 install pyarrow

# Plotting
sudo yum install graphviz -y
sudo pip-3.6 install graphviz
sudo pip-3.6 install matplotlib
sudo pip-3.6 install seaborn

# Modelling
sudo pip-3.6 install xgboost
sudo pip-3.6 install sklearn
sudo pip-3.6 install shap

# Persistance
sudo pip-3.6 install s3fs

# Testing
sudo pip-3.6 install -U pytest

# Re-Install NumPy
sudo pip-3.6 uninstall numpy -y
sudo pip-3.6 uninstall numpy -y
sudo pip-3.6 install numpy --upgrade
