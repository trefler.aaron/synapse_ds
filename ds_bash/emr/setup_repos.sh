#!/bin/bash

# Install git
sudo yum install git -y

# Setup Git configuration
git config --global user.email "trefler.aaron@gmail.com"
git config --global user.name "Aaron Trefler"

# Define directories
DIR_HOME="/home/hadoop/"
DIR_REPOS="/home/hadoop/common_git_repos"

# Create directory for repositories
mkdir ${DIR_REPOS}
sudo chmod -R 777 ${DIR_REPOS}

# Clone repositories
git clone https://gitlab.com/trefler.aaron/synapse_ds.git ${DIR_REPOS}/synapse_ds  # GitLab
sudo chmod -R 777 ${DIR_REPOS}/synapse_ds

# Zip repositories
cd ${DIR_REPOS}
sudo zip -r synapse_ds.zip synapse_ds
sudo chmod 777 synapse_ds.zip
cd ${DIR_HOME}
