#!/bin/bash
set -x

# Copy package installation script to EMR cluster
aws s3 cp \
    s3://briorep-s3-ds/Aaron-temp/setup/bootstraps/install_packages.sh \
    /home/hadoop/install_packages.sh

# Copy common repo setup script to EMR cluster
aws s3 cp \
    s3://briorep-s3-ds/Aaron-temp/setup/bootstraps/setup_repos.sh \
    /home/hadoop/setup_repos.sh

# Copy EMR setup script to EMR cluster and run
aws s3 cp \
    s3://briorep-s3-ds/Aaron-temp/setup/bootstraps/secondary_bootstrap.sh \
    /home/hadoop/secondary_bootstrap.sh \
    && sudo bash /home/hadoop/secondary_bootstrap.sh & exit 0
