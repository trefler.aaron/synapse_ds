package utilities

import org.apache.spark.sql._

class utilities_prototype {

    val spark = SparkSession.builder().appName("synapse_ds").config("spark.master", "local").getOrCreate()
    val sQLContext = spark.sqlContext

    import sQLContext.implicits._

    def create_spark_df() : DataFrame = {
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
        ).toDF("number", "word")

        return df
    }
}
